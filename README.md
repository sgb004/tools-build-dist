# Tools to build and dist components or libraries

[![Version](https://img.shields.io/badge/version-1.0.3-blue.svg)](https://www.npmjs.com/package/@wudev/tools-build-dist)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

This is a set of tools for building or distribuiting components or libraries.

## Installation

To install use npm:

```
npm install @wudev/tools-build-dist -D
```

### Commands

#### `npx vite-ctl`

Convert a project of Vite in a library.

It was inspired in the doc [Create a Component Library Fast🚀(using Vite's library mode) ](https://dev.to/receter/how-to-create-a-react-component-library-using-vites-library-mode-4lma) of [Andreas Riedmüller](https://dev.to/receter)

#### `npx wudev-replace`

Replace strings in files.

##### Config file `wudev.replace.config.js`

Example:

```javascript
const config = [
	{
		paths: ['dist'],
		types: ['php'],
		replace: [
			{
				patterns: '//--start-only-debug--+(\\s|\\S)+//--end-only-debug--',
				replacement: '',
				patternsIsRegex: true,
			},
			{
				patterns: '--last-version--',
				replacement: new Date().getTime(),
			},
		],
	},
];

export default config;
```

Where:

`config` is an array of objects that defines rules for replacing strings in files.

Each object in the `config` array represents a set of replacement rules for a specific set of files.

-   `paths`: An array of directory paths where the files to be replaced are located. In this case, files in the 'dist' directory will be targeted.

-   `types`: An array of file types that the replacements will be performed on. In this case, '.php' files will be targeted.

-   `replace`: An array of objects that define the replacement rules. Each object has three properties:
    -   `patterns`: The string or regular expression to be searched for in the files. If `patternsIsRegex` is true, `patterns` will be treated as a regular expression.
    -   `replacement`: The string that will replace the matches found for `patterns` in the files.
    -   `patternsIsRegex`: A boolean that indicates whether `patterns` should be treated as a regular expression. If true, `patterns` will be treated as a regular expression.

#### `npx wudev-cpreadme`

Copy the readme into a html, jsx or/and tsx file.

##### How to use

Add the file `examples/index.html` and the file `src/Readme.tsx` in your project.
In `examples/index.html` add the element `<div id="readme"></div>` where the content of the README.md will be placed. If the element is not added, the README.md wil be added at the beginning of the body.
For `src/Readme.tsx`, the entire content of all file will be replaced by the content of README.md.

##### Params

```
--write-in-html true|false
--write-in-jsx true|false
```

-   `--write-in-html`: Enable or disable the copy in `examples/index.html`.
-   `--write-in-jsx`: Enable or disable the copy in `src/Readme.tsx`.

##### Config file `wudev.cpreadme.config.json`

It is possible use the following configuration to change the files where README.md will be copied and also the path of README.md.

Example:

```json
{
	"readmeFile": "./README.md",
	"writeInHtml": true,
	"htmlFile": "./examples/index.html",
	"writeInJsx": true,
	"jsxFile": "./src/Readme.tsx"
}
```

-   `readmeFile`: The path of the README.md.
-   `writeInHtml`: Enable or disable the copy in the html file.
-   `htmlFile`: The path of the html where the README.md will be copied.
-   `writeInJsx`: Enable or disable the copy in the jsx file.
-   `jsxFile`: The path of the jsx or tsx where the README.md will be copied.

#### Note

If the package is installed globally, the commands will be able to used without `npx`

## Author

[sgb004](https://sgb004.com)

## License

[MIT](LICENSE)
