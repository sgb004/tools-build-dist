import { resolve } from 'path';

const pathResolve = (path) => {
	const p = path.replace(/\.\./g, '').replace(/\/\//g, '/');
	return resolve(p);
};

export default pathResolve;
