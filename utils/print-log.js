import chalk from 'chalk';

const printLog = (message, type = '') => {
	if (type === 'success') {
		console.log(chalk.green(message));
	} else if (type === 'warning') {
		console.log(chalk.yellow(message));
	} else if (type === 'error') {
		console.log(chalk.red(message));
	} else {
		console.log(message);
	}
};

export default printLog;
