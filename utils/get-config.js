import { existsSync, readFileSync } from 'fs';
import printLog from './print-log.js';
import pathResolve from './path-resolve.js';

export const getArg = (arg, def) => {
	const position = process.argv.indexOf(arg);
	let value = def;

	if (position > 1) {
		value = process.argv[position + 1];
	}

	return value;
};

const getDataConfigByJSON = (configFile) => {
	const data = readFileSync(configFile, 'utf8');
	return JSON.parse(data);
};

const getDataConfigByJS = (configFile) =>
	new Promise((resolve, reject) => {
		import(configFile)
			.then((module) => {
				resolve(module.default);
			})
			.catch((err) => {
				reject(err);
				printLog('Error loading configuration file!', 'error');
			});
	});

export const getConfig = (type, from, defaultFile, logIfConfigNotFound = true) => {
	let result = {};
	let configFile = getArg('--config', defaultFile);

	if (configFile) {
		configFile = pathResolve(`${from}/${configFile.trim()}`);
		if (existsSync(configFile)) {
			if (type === 'json') {
				result = getDataConfigByJSON(configFile);
			} else if (type === 'js') {
				result = getDataConfigByJS(configFile);
			}
		} else if (logIfConfigNotFound) {
			printLog('Configuration file not found!', 'error');
		}
	} else if (logIfConfigNotFound) {
		printLog('No config file selected!', 'error');
	}

	return result;
};

export const getConfigByJSON = (from, defaultFile, logIfConfigNotFound = true) => {
	return getConfig('json', from, defaultFile, logIfConfigNotFound);
};

export const getConfigByJS = (from, defaultFile, logIfConfigNotFound = true) => {
	return getConfig('js', from, defaultFile, logIfConfigNotFound);
};

export default getConfig;
