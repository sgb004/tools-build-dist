#!/usr/bin/env node

/*
 * @name Tool to convert a vite project in a library
 * @version 1.0.0
 * @author sgb004
 */

import { fileURLToPath } from 'url';
import { dirname, resolve } from 'path';
import { exec } from 'node:child_process';
import { existsSync } from 'node:fs';
import { copyFile, mkdir, readFile, writeFile } from 'node:fs/promises';
import JSON5 from 'json5';
import spinner from 'simple-spinner';
import chalk from 'chalk';
import printLog from '../../../utils/print-log.js';

const __dirExec = process.cwd();
const __dirname = dirname(fileURLToPath(import.meta.url));

const installDependencies = () => {
	spinner.start({
		sequence: 'dots',
		interval: 80,
		hideCursor: true,
		text:
			'Installing dev dependencies: ' +
			chalk.bold('@types/node vite-plugin-dts vite-plugin-lib-inject-css glob'),
	});

	return new Promise((resolve, reject) => {
		exec('npm i @types/node vite-plugin-dts vite-plugin-lib-inject-css glob -D', (error) => {
			spinner.stop();
			if (error) {
				reject(error);
			} else {
				printLog(
					'Packages installed: ' +
						chalk.bold('@types/node vite-plugin-dts vite-plugin-lib-inject-css glob'),
					'success'
				);
				resolve();
			}
		});
	});
};

const makeLibDir = async () => {
	if (!existsSync('./lib')) {
		await mkdir('./lib');
		printLog('Added lib directory.', 'success');
	} else {
		printLog('lib directory exists.');
	}
};

const addMainTs = async () => {
	if (!existsSync('./lib/main.ts')) {
		await writeFile('./lib/main.ts', '');
		printLog('Added lib/main.ts', 'success');
	} else {
		printLog('lib/main.ts exists.');
	}
};

const replaceViteConfigTs = async () => {
	await copyFile(resolve(__dirname, 'files/vite.config.ts'), './vite.config.ts');
	printLog('Replaced vite.config.ts', 'success');
};

const getJSONFile = async (file) => {
	const content = await readFile(file, { encoding: 'utf-8' });
	return JSON5.parse(content);
};

const writeJSONFile = async (file, data) => {
	const content = JSON.stringify(data, null, '\t');
	await writeFile(file, content);
};

const changeTsConfig = async () => {
	const config = await getJSONFile('./tsconfig.json');

	if (!config['include'].includes('lib')) {
		config['include'].push('lib');
		printLog('Added lib directory to tsconfig.', 'success');
	} else {
		printLog('tsconfig includes lib directory.');
	}

	await writeJSONFile('./tsconfig.json', config);
};

const copyTsConfigBuild = async () => {
	if (existsSync('./tsconfig-build.json')) {
		const config = await getJSONFile('./tsconfig-build.json');

		printLog('tsconfig-build.json already exists.');

		if (!config['include'].includes('lib')) {
			config['include'].push('lib');
			printLog('Added lib directory to tsconfig-build.', 'success');
			await writeJSONFile('./tsconfig-build.json', config);
		} else {
			printLog('tsconfig-build includes lib directory.');
		}
	} else {
		await copyFile(resolve(__dirname, 'files/tsconfig-build.json'), './tsconfig-build.json');
		printLog('Copied tsconfig-build.json', 'success');
	}
};

const changePackageJSON = async () => {
	const config = await getJSONFile('./package.json');

	config['scripts']['build'] = 'tsc --p ./tsconfig-build.json && vite build';
	printLog('Replaced build script in package.json', 'success');

	if (config['main'] === undefined) {
		config['main'] = 'dist/main.js';
		printLog('Added main attribute in package.json with the value dist/main.js', 'success');
	} else if (config['main'] !== 'dist/main.js') {
		printLog(
			'Main attribute already exists in package.json, consider change it to dist/main.js',
			'warning'
		);
	}

	if (config['types'] === undefined) {
		config['types'] = 'dist/main.d.ts';
		printLog('Added types attribute in package.json with the value dist/main.d.ts', 'success');
	} else if (config['types'] !== 'dist/main.d.ts') {
		printLog(
			'Types attribute already exists in package.json, consider change it to dist/main.d.ts',
			'warning'
		);
	}

	if (config['files'] === undefined) {
		config['files'] = ['dist'];
		printLog('Added files attribute in package.json with the value dist', 'success');
	} else if (Array.isArray(config['files'])) {
		if (!config['files'].includes('dist')) {
			config['files'].push('dist');
			printLog('Added dist directory in files attribute in package.json', 'success');
		}
	} else {
		printLog(
			'It was not possible to add the dist directory in the files attribute in the package.json',
			'error'
		);
	}

	if (config['peerDependencies'] === undefined) {
		config['peerDependencies'] = config['dependencies'];
		delete config['dependencies'];
		printLog(
			'Dependencies were moved to peerDependencies, please check if the changes were correct.',
			'warning'
		);
	} else if (
		typeof config['peerDependencies'] === 'object' &&
		typeof config['dependencies'] === 'object'
	) {
		config['peerDependencies'] = { ...config['dependencies'], ...config['peerDependencies'] };
		delete config['dependencies'];
		printLog(
			'Dependencies were moved to peerDependencies, please check if the changes were correct.',
			'warning'
		);
	} else {
		printLog('It was not possible to move dependencies to peerDependencies', 'error');
	}

	if (config['sideEffects'] === undefined) {
		config['sideEffects'] = ['**/*.css'];
		printLog('Added sideEffects attribute in package.json with the value **/*.css', 'success');
	} else if (Array.isArray(config['sideEffects'])) {
		if (!config['sideEffects'].includes('**/*.css')) {
			config['sideEffects'].push('**/*.css');
			printLog('Added **/*.css in sideEffects attribute in package.json', 'success');
		}
	} else {
		printLog(
			'It was not possible to add **/*.css in the files sideEffects in the package.json',
			'error'
		);
	}

	await writeJSONFile('./package.json', config);
};

const copySrcViteEnv = async () => {
	if (existsSync('./lib/vite-env.d.ts')) {
		printLog('vite-env.d.ts in lib directory exists');
	} else {
		await copyFile('./src/vite-env.d.ts', './lib/vite-env.d.ts');
		printLog('Copied src/vite-env.d.ts to lib/vite-env.d.ts', 'success');
	}
};

const start = () => {
	console.log(chalk.bold('\nWelcome to Vite convert to lib\n'));
};

const end = () => {
	console.log(chalk.bgGreen.bold('\n Ready!!! \n'));
	printLog('Now you can move the components from src directory to lib directory.');
	printLog('Remember to add the componentes in the lib/main.ts');
	printLog(
		'You can check about the changes in https://dev.to/receter/how-to-create-a-react-component-library-using-vites-library-mode-4lma'
	);
	printLog('\nThank you Andreas Riedmüller!\n');
};

start();
await installDependencies();
await makeLibDir();
await addMainTs();
await replaceViteConfigTs();
await changeTsConfig();
await copyTsConfigBuild();
await changePackageJSON();
await copySrcViteEnv();
end();
