#!/usr/bin/env node

/*
 * @name
 * @version 1.0.0
 * @author sgb004
 */

import { readdirSync, readFileSync, writeFileSync } from 'fs';
import { resolve } from 'path';
import { getConfigByJS } from '../utils/get-config.js';
import printLog from '../utils/print-log.js';
import pathResolve from '../utils/path-resolve.js';

const __dirExec = process.cwd();

const checkConfig = (config) => {
	let result = true;

	if (!Array.isArray(config.paths)) {
		result = false;
		printLog('Error: paths is not an array', 'error');
	} else if (!Array.isArray(config.types)) {
		result = false;
		printLog('Error: types is not an array', 'error');
	} else if (!Array.isArray(config.replace)) {
		result = false;
		printLog('Error: replace is not an array', 'error');
	}

	return result;
};

const getDirs = (paths) => {
	const listOfPaths = [...paths];
	const dirs = [];

	do {
		const path = listOfPaths.shift();
		const fullPath = pathResolve(`${__dirExec}/${path}`);
		const subDirs = readdirSync(fullPath, { withFileTypes: true })
			.filter((dirent) => dirent.isDirectory())
			.map((dirent) => `${path}/${dirent.name}`);

		listOfPaths.push(...subDirs);

		dirs.push(fullPath);
	} while (listOfPaths.length > 0);

	return dirs;
};

const replaceContent = (rules, content) => {
	let result = content;

	for (const rule of rules) {
		const patterns = rule.patternsIsRegex ? new RegExp(rule.patterns, 'gi') : rule.patterns;
		result = result.replaceAll(patterns, rule.replacement);
	}

	return result;
};

const replace = (config) => {
	const dirs = getDirs(config.paths);

	for (const dir of dirs) {
		const files = readdirSync(dir, { withFileTypes: true }).filter(
			(dirent) =>
				dirent.isFile() &&
				config.types.some((type) => dirent.name.match(new RegExp(`.*\.${type}$`)))
		);

		for (const file of files) {
			const path = resolve(dir, file.name);
			const content = readFileSync(path, 'utf8');
			writeFileSync(path, replaceContent(config.replace, content));
		}
	}
};

const init = (config) => {
	if (Array.isArray(config)) {
		const isConfigValid = config.every(checkConfig);
		if (isConfigValid) {
			for (const item of config) {
				replace(item);
			}
			printLog('Replace Done! \n', 'success');
		}
	} else {
		printLog('Error: Config file is not an array', 'error');
	}
};

printLog('');
getConfigByJS(__dirExec, 'wudev.replace.config.js')
	.then((config) => {
		init(config);
	})
	.catch((error) => {
		throw error;
	});
