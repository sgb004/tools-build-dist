#!/usr/bin/env node

/*
 * @name cpreadme
 * @version 1.0.0
 * @author sgb004
 */

import { readFileSync, writeFileSync } from 'fs';
import Showdown from 'showdown';
import * as cheerio from 'cheerio';
import { getArg, getConfigByJSON } from '../utils/get-config.js';
import pathResolve from '../utils/path-resolve.js';

const __dirExec = process.cwd();

const cleanReadmeHtml = (readmeHtml) => {
	readmeHtml = readmeHtml.replace(/\$/g, '&#36;');
	readmeHtml = readmeHtml.replace(/{/g, '&#123;');
	readmeHtml = readmeHtml.replace(/}/g, '&#125;');

	readmeHtml = readmeHtml.replace(/<code(.*?)>(.*?)<\/code>/gs, (match, attrs, codeContent) => {
		const codeContentWithBr = codeContent.replace(/\n/g, '<br/>');
		return `<code>${codeContentWithBr}</code>`;
	});

	readmeHtml = readmeHtml.replace(
		/<\!-- start-remove-in-html -->(.*?)<\!-- end-remove-in-html -->/gs,
		(match, codeContent) => ''
	);

	return readmeHtml;
};

const getReadmeHtml = () => {
	const readme = readFileSync(pathResolve(`${__dirExec}/${config.readmeFile}`), 'utf8');
	const convert = new Showdown.Converter({ noHeaderId: true });
	const html = convert.makeHtml(readme);
	return cleanReadmeHtml(html);
};

const writeIndexHtml = (readmeHtml) => {
	const indexHtml = readFileSync(pathResolve(`${__dirExec}/${config.htmlFile}`), 'utf8');
	const $ = cheerio.load(indexHtml);
	const examples = $('#examples');
	let readme = $('#readme');

	if (readme.length === 0) {
		$('body').prepend('<div id="readme"></div>');
		readme = $('#readme');
	}

	readmeHtml = readmeHtml.replace(
		/<!-- examples -->/g,
		`<div id="examples">${examples.html()}</div>`
	);

	examples.remove();
	readme.html(readmeHtml);

	writeFileSync(pathResolve(`${__dirExec}/${config.htmlFile}`), $.html());
	console.log(`${config.readmeFile} copied to ${config.htmlFile}`);
};

const writeReadmeJsx = (readmeHtml) => {
	let html = readmeHtml.replace(/<!-- examples -->/g, '<Examples />');
	html = html.replace(/<!--/g, '{/*');
	html = html.replace(/-->/g, '*/}');

	html = `import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	${html}
	</div>
);

export default Readme;
	`;

	writeFileSync(pathResolve(`${__dirExec}/${config.jsxFile}`), html);
	console.log(`${config.readmeFile} copied to ${config.jsxFile}`);
};

const writeInIndexHtml = getArg('--write-in-html', true);
const writeInReadmeJsx = getArg('--write-in-jsx', true);
const configData = getConfigByJSON(__dirExec, 'wudev.cpreadme.config.json', false);
const config = {
	readmeFile: './README.md',
	writeInHtml: writeInIndexHtml == true || writeInIndexHtml == 'true',
	htmlFile: './examples/index.html',
	writeInJsx: writeInReadmeJsx == true || writeInReadmeJsx == 'true',
	jsxFile: './src/Readme.tsx',
	...configData,
};

const readmeHtml = getReadmeHtml();

if (config.writeInHtml) {
	writeIndexHtml(readmeHtml);
}

if (config.writeInJsx) {
	writeReadmeJsx(readmeHtml);
}
